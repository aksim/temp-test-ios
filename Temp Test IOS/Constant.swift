//
//  Constant.swift
//  IBMRemote
//
//  Created by Omar Aksmi on 9/16/19.
//  Copyright © 2019 IBM. All rights reserved.
//

import Foundation

struct Constant {
    
    static let REMOTE_BASE = "https://www.mocky.io/v2/"
    
}
