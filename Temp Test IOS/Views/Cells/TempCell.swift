//
//  TempCell.swift
//  Temp Test IOS
//
//  Created by Omar Aksmi on 10/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class TempCell: UITableViewCell {

    @IBOutlet weak var degree: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setup(temp: TempViewModel){
        self.degree.text = "\(temp.temp.temp)"
        self.backgroundColor = temp.isTheClosest ? .red : .white
    }
    
}
