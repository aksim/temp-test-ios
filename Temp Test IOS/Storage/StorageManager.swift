//
//  StorageManager.swift
//  Temp Test IOS
//
//  Created by Omar Aksmi on 10/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import Foundation
import SQLite3

class StorageManager {
    
    static let shared = StorageManager()
    private var db: OpaquePointer?
    private var fileURL:URL?
    
    func initDataBase(){
        self.fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("Temp.sqlite")
        
        open()
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS temps (id INTEGER PRIMARY KEY AUTOINCREMENT, temp INTEGER)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
    }
    
    private func open(){
        
        if sqlite3_open(fileURL?.path, &db) != SQLITE_OK {
            print("error opening database")
        }
    }
    
    
    private func close(){
        
    }
    
    
    func addTemps(temps: [Temp], completion: @escaping () -> ()){
        for temp in temps{
            self.addTemp(temp: temp)
        }
        completion()
    }
    
    
    func addTemp(temp: Temp){
        var stmt: OpaquePointer?
        let queryString = "INSERT INTO temps (temp) VALUES (?)"
        //preparing the query
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
        
        
        if sqlite3_bind_double(stmt, 1, temp.temp) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding temp: \(errmsg)")
            return
        }
        
        //executing the query to insert values
        if sqlite3_step(stmt) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting temp: \(errmsg)")
            return
        }
        
    }
    
    func clearTemps(){
        var stmt: OpaquePointer?
        let queryString = "DELETE FROM temps"
        //preparing the query
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing temps: \(errmsg)")
            return
        }
        
        
        //executing the query to insert values
        if sqlite3_step(stmt) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure deleting temps: \(errmsg)")
            return
        }
        
        
        
        //displaying a success message
        print("Temp deleted successfully")
    }
    
    func findAllTemps() -> [Temp]? {
        //this is our select query
        let queryString = "SELECT * FROM temps"
        
        //statement pointer
        var stmt:OpaquePointer?
        
        //preparing the query
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing select: \(errmsg)")
            return nil
        }
        
        var tempList: [Temp] = []
        //traversing through all the records
        while(sqlite3_step(stmt) == SQLITE_ROW){
            let id = sqlite3_column_int(stmt, 0)
            let temp = sqlite3_column_double(stmt, 1)
            
            //adding values to list
            tempList.append(Temp(id: Int(id), temp: Double(temp)))
        }
        return tempList
        
    }
    
}
