//
//  ViewController.swift
//  Temp Test IOS
//
//  Created by Omar Aksmi on 10/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let cellId = "TempCell"
    let tempsViewModel = TempsViewModel()
    @IBOutlet weak var tempTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        StorageManager.shared.initDataBase()
        
        
        self.tempTableView.delegate = self
        self.tempTableView.dataSource = self
        self.tempTableView.register(UINib(nibName: self.cellId, bundle: nil), forCellReuseIdentifier: self.cellId)
        
        self.tempsViewModel.loadData {
            DispatchQueue.main.async {
                self.tempTableView.reloadData()
            }
        }
        
        
    }
    
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tempsViewModel.temps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath) as! TempCell
        cell.setup(temp: self.tempsViewModel.temps[indexPath.row])
        return cell
    }
    
    
    
    
    
}
