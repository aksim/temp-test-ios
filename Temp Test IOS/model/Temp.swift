//
//  Temp.swift
//  Temp Test IOS
//
//  Created by Omar Aksmi on 10/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import Foundation

struct Temp {
    let id: Int
    let temp: Double
}


extension Temp: Decodable {
    enum TempKeys: String, CodingKey{
        
        case id = "id"
        case temp = "temp"
    }
    
    
    init(from decoder: Decoder) throws {
        
        let TempContainer = try decoder.container(keyedBy: TempKeys.self)
        
        id = try TempContainer.decode(Int.self, forKey: .id)
        temp = try TempContainer.decode(Double.self, forKey: .temp)
    }
}
