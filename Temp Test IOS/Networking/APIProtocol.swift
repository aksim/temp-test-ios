//
//  APIProtocol.swift
//  IBMRemote
//
//  Created by Omar Aksmi on 9/16/19.
//  Copyright © 2019 IBM. All rights reserved.
//

import Foundation

protocol APIProtocol {
    var baseURL: URL { get }
    var path: String { get }
}
