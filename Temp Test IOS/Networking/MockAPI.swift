//
//  MockAPI.swift
//  IBMRemote
//
//  Created by Omar Aksmi on 9/16/19.
//  Copyright © 2019 IBM. All rights reserved.
//

import Foundation

enum MockAPI: APIProtocol {
    case tempList
}

extension MockAPI {
    
    var baseURL: URL {
        return URL(string: Constant.REMOTE_BASE)!
    }
    
    var path: String {
        switch self {
        case .tempList:
            return "5d9afc5a3200000f002ae4a5"
        }
    }
    
}
