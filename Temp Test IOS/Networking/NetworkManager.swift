//
//  NetworkManager.swift
//  IBMRemote
//
//  Created by Omar Aksmi on 9/16/19.
//  Copyright © 2019 IBM. All rights reserved.
//

import Foundation

class NetworkManager {
    
    
    static let shared = NetworkManager()
    private let session: URLSession
    
    // Initialization
    
    private init() {
        self.session = URLSession(configuration: .default)
    }
    
    private func performRequest(method:HTTPMethod, endpoint:APIProtocol,parameters:HTTPParameters,includes: HTTPHeaders, completion: @escaping (Result<Data>) -> () ){
        do{
            
            let urlString = endpoint.baseURL.appendingPathComponent(endpoint.path).absoluteString.removingPercentEncoding ?? ""
            let request = try HTTPNetworkRequest.configureHTTPRequest(from: urlString, with: [:], includes: includes, contains: parameters, and: method)
            session.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        completion(Result.success(unwrappedData))
                    case .failure:
                        completion(Result.failure(HTTPNetworkError.badRequest))
                    }
                }
                }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.failed))
        }
    }
    
    func get(endpoint:APIProtocol,parameters:HTTPParameters,includes: HTTPHeaders, completion: @escaping (Result<Data>) -> () ) {
        
        var headers = ["Content-Type": "application/json"]
        headers.merge(includes){(current, _) in current}
        performRequest(method: .get, endpoint: endpoint, parameters: parameters, includes: headers) { (result) in
            completion(result)
        }
    }
    
    func post(endpoint:APIProtocol,parameters:HTTPParameters,includes: HTTPHeaders, completion: @escaping (Result<Data>) -> () ) {
        
        var headers = ["Content-Type": "application/json"]
        headers.merge(includes){(current, _) in current}
        performRequest(method: .post, endpoint: endpoint, parameters: parameters, includes: headers) { (result) in
            completion(result)
        }
    }
    
    
    
}
