//
//  TempServices.swift
//  Temp Test IOS
//
//  Created by Omar Aksmi on 10/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import Foundation

class TempServices {
    
    static func templist(completion: @escaping (Result<[Temp]>) -> () ){
        
        NetworkManager.shared.get(endpoint: MockAPI.tempList, parameters: [:], includes: [:]) { (responseData) in
            switch responseData {
            case .success(let data):
                let result = try? JSONDecoder().decode([Temp].self, from: data)
                if let user = result {
                    completion(Result.success(user))
                }else {
                    completion(Result.failure(HTTPNetworkError.decodingFailed))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
            
            
        }
    }
    
    
    
    
}
