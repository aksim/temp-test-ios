//
//  TempsViewModel.swift
//  Temp Test IOS
//
//  Created by Omar Aksmi on 10/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import Foundation

class TempsViewModel {
    
    var temps: [TempViewModel] = []
    
    func closeToZero(temps: [Temp]) -> Double{
        
        let x:Double = 0.0
        let sorted = temps.sorted(by: {  abs($0.temp - x) <= abs($1.temp - x) })
     
        if sorted.count > 1 {
            return abs(sorted[0].temp) == abs(sorted[1].temp) ? max(sorted[0].temp, sorted[1].temp) : sorted[0].temp
        }
        return sorted.first?.temp ?? 0
    }
    
    func loadData(completion: @escaping () -> ()){
        TempServices.templist { (result) in
            switch result {
            case .success(let tempList):
                StorageManager.shared.clearTemps()
                StorageManager.shared.addTemps(temps: tempList) {
                    let closest = self.closeToZero(temps: tempList)
                    self.temps = {
                        var models: [TempViewModel] = []
                        for temp in tempList {
                            models.append(TempViewModel(temp: temp, closest: closest))
                        }
                        return models
                    }()
                }
                completion()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
}
