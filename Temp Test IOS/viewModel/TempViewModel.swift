//
//  TempViewModel.swift
//  Temp Test IOS
//
//  Created by Omar Aksmi on 10/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import Foundation

class TempViewModel {
    
    let temp: Temp
    let isTheClosest: Bool
    
    init(temp:Temp, closest: Double) {
        self.temp = temp
        self.isTheClosest = (temp.temp == closest)
    }
    
}
